﻿using Prism.Commands;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using ToDoPrism.Models;
using ToDoPrism.Repositories;
using Windows.UI.Xaml.Controls;

namespace ToDoPrism.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {

        private IDataRepository _dataRepository;
        private TaskToDo TaskItem { get; set; }

        public MainPageViewModel(IDataRepository dataRepository) 
        {
            _dataRepository = dataRepository;

            TaskToDo = new TaskToDo { done = false };

            InitializeCommands();
        }

        private void InitializeCommands()
        {
            AddTaskToDo = new DelegateCommand(() => 
            {
                _dataRepository.Save(new TaskToDo() { title = TaskToDo.title, done = TaskToDo.done });
                UpdateGridView();
            });

            SelectTask = new DelegateCommand<TaskToDo>(SelectTaskItem);
            CompleteTask = new DelegateCommand(CompleteTaskToDo);

        }

        private void CompleteTaskToDo()
        {
            TaskItem.done = true;
            _dataRepository.UpdateTask(TaskItem);
            UpdateGridView();
        }

        private void SelectTaskItem(TaskToDo task)
        {
            TaskItem = task;
        }

        private void UpdateGridView() 
        {
                ToDos = new ObservableCollection<TaskToDo>(_dataRepository.ListTasks());
        }

        #region Commands
        public DelegateCommand AddTaskToDo { get; private set; }
        public DelegateCommand<TaskToDo> SelectTask { get; private set; }
        public DelegateCommand CompleteTask { get; private set; }
        #endregion

        #region Properties
        private ObservableCollection<TaskToDo> _toDos;
        public ObservableCollection<TaskToDo> ToDos 
        {
            get { return _toDos;}
            set { SetProperty(ref _toDos, value); }
        }

        private TaskToDo _taskToDo;
        public TaskToDo TaskToDo
        {
            get { return _taskToDo; }
            set { SetProperty(ref _taskToDo, value); }
        }


        #endregion

        #region overrides
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            base.OnNavigatedTo(e, viewModelState);

            UpdateGridView();
        }

        public override void OnNavigatingFrom(NavigatingFromEventArgs e, Dictionary<string, object> viewModelState, bool suspending)
        {
            base.OnNavigatingFrom(e, viewModelState, suspending);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
        }

        protected override void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            base.OnPropertyChanged(propertyExpression);
        }

        protected override bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            return base.SetProperty(ref storage, value, propertyName);
        }

        
        #endregion
    }
}
