﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoPrism.Models;

namespace ToDoPrism.Repositories
{
    public class DataRepository : IDataRepository
    {

        private List<TaskToDo> _data;

        public DataRepository()
        {
            _data = new List<TaskToDo>()
                    {
                        new TaskToDo() { title="Task 1", done = false},
                        new TaskToDo() { title="Task 2", done = false},
                        new TaskToDo() { title="Task 3", done = false},
                        new TaskToDo() { title="Task 4", done = false},
                        new TaskToDo() { title="Task 5", done = true},
                        new TaskToDo() { title="Task 5 b", done = false},
                        new TaskToDo() { title="Task 6", done = false},
                    };
        }

        public TaskToDo GetTaskByID(int id)
        {
            if(_data.ToArray().Length > 0)
                return _data[id];

            return null;
        }

        public int GetTaskId(TaskToDo task)
        {
            var id = _data.FindIndex(t => t == task);
            //Ajustar nao tem como vir null
            if (id == null)
                return -1;

            return id;
        }

        public List<TaskToDo> ListTasks()
        {
            return _data.Where(t => t.done == false).ToList();
        }

        public void Save(TaskToDo task)
        {
              if (task != null)
                _data.Add(task);
        }

        public void UpdateTask(TaskToDo task) 
        {
            var id = GetTaskId(task);

            _data[id].done = task.done;
            _data[id].title = task.title;
        }
    }
}
