﻿using System.Collections.Generic;
using ToDoPrism.Models;

namespace ToDoPrism.Repositories
{
    public interface IDataRepository
    {
        void Save(TaskToDo task);
        List<TaskToDo> ListTasks();
        int GetTaskId(TaskToDo task);
        TaskToDo GetTaskByID(int id);
        void UpdateTask(TaskToDo task);
    }
}