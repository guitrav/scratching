﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoPrism.Models
{
    public class TaskToDo : BindableBase
    {
        public TaskToDo()
        {
        }

        private string Title;
        public string title 
        { 
            get { return Title; }
            set { SetProperty(ref Title, value); } 
        }

        private bool Done;
        public bool done
        {
            get { return Done; }
            set { SetProperty(ref Done, value); }
        }

    }
}
