﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Selector
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        ObservableCollection<Person> _people = new ObservableCollection<Person>();

        public MainPage()
        {
            this.InitializeComponent();
            FillListPeople();
        }


        public async void FillListPeople()
        {
            var client = new HttpClient();
            var response = await client.GetAsync("http://localhost:55474/api/people");
            var text = await response.Content.ReadAsStringAsync();
            var people = JsonConvert.DeserializeObject<Person[]>(text);

            foreach (var person in people) 
                _people.Add(person);
                 

            ListPeople.ItemsSource = _people;
        }
    }
}
