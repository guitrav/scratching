﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selector
{
    public class Person
    {
        public string FullName { get; set; }
        public string NickName { get; set; }
        public int Age { get; set; }

    }
}
