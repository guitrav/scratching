﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Selector
{
    public class PersonDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate PersonTemplateGreen { get; set; }
        public DataTemplate PersonTemplateRed { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var person = (Person)item;

            if (person.Age > 30)
                return PersonTemplateRed;
            else if (person.Age <= 30)
                return PersonTemplateGreen;
            
            return PersonTemplateRed;
        }

    }
}
