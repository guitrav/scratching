﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Selector_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PeopleController : ControllerBase
    {
        // GET: /People
        [HttpGet]
        public IList<Person> Get()
        {
            return new People().All.ToList();
        }

    }

    public class People
    {
        public List<Person> All
        {
            get
            {
                return new List<Person>()
                    {
                        new Person {FullName = "Daniel Barros", NickName = "Urso", Age = 27 },
                        new Person {FullName = "João Paulo", NickName = "JP", Age = 29 },
                        new Person {FullName = "José Carlos", NickName = "Zoeiro", Age = 28 },
                        new Person {FullName = "Felipe Duarte", NickName = "Irmão de Zeca", Age = 29 },
                        new Person {FullName = "Antônio Cavalcante", NickName = "Toin", Age = 35 },
                        new Person {FullName = "Leonardo Martins", NickName = "Ninja", Age = 45 },
                        new Person {FullName = "Fernando Lúcio", NickName = "Adnet", Age = 17 }
                    };
            }
        }        
    }

    public class Person
    {
        public string FullName { get; set; }
        public string NickName { get; set; }
        public int Age { get; set; }

    }
}
