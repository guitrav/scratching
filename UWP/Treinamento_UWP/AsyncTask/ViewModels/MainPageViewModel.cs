﻿using Prism.Commands;
using Prism.Windows.Mvvm;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncTask.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public DelegateCommand ProgressBarsUpDateCommand { get; private set; }
        public DelegateCommand ProgressBarsCancelCommand { get; private set; }

        const int PROGRESSBARMAXIMUM = 100;

        CancellationTokenSource _cts;

        #region Properties
        public int _raceOne;
        public int RaceOne
        {
            get { return _raceOne; }
            set { SetProperty(ref _raceOne, value); }
        }

        public int _raceTwo;
        public int RaceTwo
        {
            get { return _raceTwo; }
            set { SetProperty(ref _raceTwo, value); }
        }

        public int _raceThree;
        public int RaceThree
        {
            get { return _raceThree; }
            set { SetProperty(ref _raceThree, value); }
        }

        public int _speedOne;
        public int SpeedOne
        {
            get { return _speedOne; }
            set { SetProperty(ref _speedOne, value); }
        }

        public int _speedTwo;
        public int SpeedTwo
        {
            get { return _speedTwo; }
            set { SetProperty(ref _speedTwo, value); }
        }

        public int _speedThree;
        public int SpeedThree
        {
            get { return _speedThree; }
            set { SetProperty(ref _speedThree, value); }
        }
        #endregion

        public MainPageViewModel()
        {
            InitializeCommands();
            ResetRaces();
        }

        private void InitializeCommands()
        {
            ProgressBarsUpDateCommand = new DelegateCommand(() =>
            {
                _cts = new CancellationTokenSource();
                ProgressBarsUpDate();
            });
            ProgressBarsCancelCommand = new DelegateCommand(() =>
            {
                ProgressBarsCancelUpDate();
            });
        }

        private void ProgressBarsCancelUpDate()
        {
            if (_cts != null)
            {
                _cts.Cancel();
            }
        }

        private async void ProgressBarsUpDate()
        {
            ResetRaces();

            await Task.WhenAll(new Task[] {
                UpdateProgressBar(SpeedOne, new Progress<int>(p => RaceOne = p)),
                UpdateProgressBar(SpeedTwo, new Progress<int>(p => RaceTwo = p)),
                UpdateProgressBar(SpeedThree, new Progress<int>(p => RaceThree = p))
            });

            _cts = new CancellationTokenSource();

        }

        private void ResetRaces()
        {
            RaceOne = 0;
            RaceTwo = 0;
            RaceThree = 0;
        }

        private async Task UpdateProgressBar(int speed, IProgress<int> progress)
        {
            for (int i = 0; i <= PROGRESSBARMAXIMUM; i++)
            {
                if (_cts.IsCancellationRequested)
                    break;

                await Task.Delay(speed);
                progress.Report(i);
            }
        }

    }
}
