﻿namespace PhoneBook
{
    public class PageTokens
    {
        public static string Main = "Main";

        public static string AddContact = "AddContact";

        public static string DetailContact = "DetailContact";

        public static string EditContact = "EditContact";
    }
}