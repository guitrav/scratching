﻿using System.Collections.Generic;
using PhoneBook.Models;

namespace PhoneBook.Repositories
{
    public interface IDataRepository
    {
        void Save(Contact contact);
        void Delete(int contactId);
        List<Contact> ListContacts();
        int GetContactId(Contact contact);
        Contact GetContactByID(int id);
        void UpdateContact(int contactId, Contact contact);
        List<Contact> SearchContact(string query);
    }
}