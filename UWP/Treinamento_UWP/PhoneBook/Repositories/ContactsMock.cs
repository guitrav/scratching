﻿using PhoneBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Repositories
{
    public class ContactsMock
    {


        public Contact getContact() 
        {
            return new Contact() { Name = "Fulando de Tal", Nickname = "Tal", Age = "30", Phone = "4444-4444" };
        }

        public List<Contact> getAllContacts()
        {
            return new List<Contact>() 
            {
                new Contact() { Name = "Fulando de Tal", Nickname = "Tal", Age = "30", Phone = "4444-4444" },
                new Contact() { Name = "Sicrano de Tal", Nickname = "Sic", Age = "36", Phone = "5555-5555" },
                new Contact() { Name = "Beltrano de Tal", Nickname = "Bel", Age = "25", Phone = "6666-6666" },
                new Contact() { Name = "Babu Bob", Nickname = "Bob", Age = "33", Phone = "7777-7777" }
            };
        }
    }
}
