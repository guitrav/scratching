﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneBook.Models;

namespace PhoneBook.Repositories
{
    public class DataRepository : IDataRepository
    {

        public List<Contact> _data;

        public DataRepository()
        {
            _data = new ContactsMock().getAllContacts();
        }

        public Contact GetContactByID(int id)
        {
            if(_data.ToArray().Length > 0)
                return _data[id];

            return null;
        }

        public int GetContactId(Contact contact)
        {
            var id = _data.FindIndex(t => t == contact);

            return id;
        }

        public List<Contact> ListContacts()
        {
            return _data.ToList();
        }

        public void Save(Contact contact)
        {
              if (contact != null)
                _data.Add(contact);

        }

        public void Delete(int contactId)
        {
                _data.RemoveAt(contactId);
        }

        public List<Contact> SearchContact(string query)
        {
            query = query.ToLowerInvariant();
            return _data.Where(obj => obj.Name.ToLowerInvariant().Contains(query)).ToList();
        }

        public void UpdateContact(int contactId, Contact contact)
        {
            _data[contactId].Name = contact.Name;
            _data[contactId].Nickname = contact.Nickname;
            _data[contactId].Age = contact.Age;
            _data[contactId].Phone = contact.Phone;
        }
    }
}
