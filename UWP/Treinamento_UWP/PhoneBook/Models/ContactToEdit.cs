﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Models
{
    public class ContactToEdit
    {
        public int ContactID { get; set; }
        public Contact SelectedContact { get; set; }
    }
}
