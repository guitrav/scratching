﻿using PhoneBook.Utils;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Models
{
    public class Contact : ValidatableBindableBase
    {
        
        private string _name;
        [Required(ErrorMessage = "Name is required.")]
        public string Name 
        {
          get { return _name; }
          set { SetProperty(ref _name, value); } 
        }
        
        private string _nickname;
        [Required(ErrorMessage = "Nickname is required.")]
        public string Nickname
        {
            get { return _nickname; }
            set { SetProperty(ref _nickname, value); }
        }
        
        private string _age;
        [Required(ErrorMessage = "Age is required.")]
        [NumericAttribute(ErrorMessage = "Must be number.")]
        [MoreThanZero(ErrorMessage = "Must be More than zero.")]
        public string Age
        {
            get { return _age; }
            set { SetProperty(ref _age, value); }
        }

        private string _phone;
        [Required(ErrorMessage = "Phone is required.")]
        public string Phone
        {
            get { return _phone; }
            set { SetProperty(ref _phone, value); }
        }

        public Contact ShallowCopy()
        {
            return (Contact)this.MemberwiseClone();
        }

        public void ValidateFields()
        {
            ValidateProperties();
        }
    }
}
