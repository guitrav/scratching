﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.Models
{
    public class Error
    {
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
