﻿using PhoneBook.Models;
using PhoneBook.Repositories;
using Prism.Commands;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.ViewModels
{
    public class AddContactPageViewModel : ViewModelBase
    {
        private INavigationService _navigation;
        private IDataRepository _dataRepository;

        private ObservableCollection<Error> _errors;
        public ObservableCollection<Error> Errors
        {
            get { return _errors; }
            set { SetProperty(ref _errors, value); }
        }

        private Contact _contact;
        public Contact Contact
        {
            get { return _contact; }
            set 
            { 
                SetProperty(ref _contact, value);
                _contact.ValidateProperties();
            }
        }

        public DelegateCommand SaveContactCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }

        public AddContactPageViewModel(INavigationService navigation, IDataRepository dataRepository)
        {
            _navigation = navigation;
            _dataRepository = dataRepository;

            _contact = new Contact();
            Errors = new ObservableCollection<Error>();

            InitializeCommnads();
        }

        private void InitializeCommnads()
        {
            SaveContactCommand = new DelegateCommand(() =>
            {
                _contact.ValidateProperties();

                if (_contact.Errors.Errors.Count == 0)
                {
                    _dataRepository.Save(Contact);
                    _navigation.GoBack();
                }
                else
                {
                    fillErros(Contact.GetAllErrors());
                }
            });

            CancelCommand = new DelegateCommand(() => _navigation.GoBack(), () => _navigation.CanGoBack());
        }

        private void fillErros(ReadOnlyDictionary<string, ReadOnlyCollection<string>> readOnlyDictionary)
        {
            Errors.Clear();

            foreach (var error in readOnlyDictionary)
            {
                Errors.Add(new Error() { Field = error.Key, Value = error.Value[0].ToString() });
            }
        }

    }
}
