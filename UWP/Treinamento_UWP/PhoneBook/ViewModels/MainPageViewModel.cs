﻿using PhoneBook.Models;
using PhoneBook.Repositories;
using Prism.Commands;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhoneBook.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public DelegateCommand AddContactCommand { get; private set; }
        public DelegateCommand<Contact> ShowContactDetailsCommand { get; private set; }
        public DelegateCommand<string> SearchContactCommand { get; private set; }

        public IDataRepository _dataRepository;
        private INavigationService _navigation;
        private ObservableCollection<GroupInfosList> _contacts = new ObservableCollection<GroupInfosList>();
        public ObservableCollection<GroupInfosList> Contacts
        {
            get { return _contacts; }
            set { SetProperty(ref _contacts, value); }
        }

        private string searchContactQuery;
        public string SearchContactQuery
        {
            get { return searchContactQuery; }
            set
            {
                SetProperty(ref searchContactQuery, value);
                SearchContact(searchContactQuery);
            }
        }



        public MainPageViewModel(INavigationService navigation, IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
            _navigation = navigation;

            InitializeCommands();
            GroupingContactsByName(_dataRepository.ListContacts());
        }

        private void InitializeCommands()
        {
            AddContactCommand = new DelegateCommand(AddContact);
            SearchContactCommand = new DelegateCommand<string>(SearchContact);
            ShowContactDetailsCommand = new DelegateCommand<Contact>(NavigateContactDetails);

        }


        private void AddContact()
        {
            _navigation.Navigate(PageTokens.AddContact, null);
        }

        private void SearchContact(string name)
        {
            GroupingContactsByName(_dataRepository.SearchContact(name));
        }

        private void NavigateContactDetails(Contact contact)
        {
            _navigation.Navigate(PageTokens.DetailContact, contact);
        }

        private void GroupingContactsByName(List<Contact> list)
        {
            Contacts.Clear();

            if (list == null)
                list = _dataRepository.ListContacts();


            
            var groupedContacts = list.Select(p => p).GroupBy( g => g.Name.Substring(0,1).ToUpper(),
                (letter, names) => new { GroupeLetter = letter, Names = names.OrderBy(n => n.Name).ToList() })
                .OrderBy(l => l.GroupeLetter);


            foreach (var letter in groupedContacts)
            {
                GroupInfosList contactsListByName = new GroupInfosList();
                contactsListByName.Key = letter;

                foreach (var contact in letter.Names)
                {
                    contactsListByName.Add(contact);
                }

                Contacts.Add(contactsListByName);
            }
        }

        public override void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            base.OnNavigatedTo(e, viewModelState);

            GroupingContactsByName(_dataRepository.ListContacts());
        }

    }
}
