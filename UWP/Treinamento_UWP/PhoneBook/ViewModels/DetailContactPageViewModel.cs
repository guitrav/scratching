﻿using PhoneBook.Models;
using PhoneBook.Repositories;
using Prism.Commands;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.ViewModels
{
    public class DetailContactPageViewModel : ViewModelBase
    {
        private INavigationService _navigation;
        private IDataRepository _dataRepository;

        private int contactIndex;

        private Contact _selectedContact;
        public Contact SelectedContact
        {
            get { return _selectedContact; }
            set 
            { 
                
                SetProperty(ref _selectedContact, value);
                _selectedContact.ValidateProperties();
            }
        }

        public DelegateCommand<Contact> EditContactCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }
        public DelegateCommand DeleteContactCommand { get; private set; }

        public DetailContactPageViewModel(INavigationService navigation, IDataRepository dataRepository)
        {
            _navigation = navigation;
            _dataRepository = dataRepository;

            InitializeCommnads();
        }

        private void InitializeCommnads()
        {
            EditContactCommand = new DelegateCommand<Contact>(EditContact);

            DeleteContactCommand = new DelegateCommand(() =>
            {
                _dataRepository.Delete(contactIndex);
                _navigation.GoBack();
            });

            CancelCommand = new DelegateCommand(() => _navigation.GoBack(), () => _navigation.CanGoBack());
        }

        private void EditContact(Contact contact)
        {
            _navigation.Navigate(PageTokens.EditContact, contact);
        }


        public override void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            base.OnNavigatedTo(e, viewModelState);

            if (e.Parameter != null && e.Parameter is Contact)
            {
                var contact = (Contact)e.Parameter;
                contactIndex = _dataRepository.GetContactId(contact);

                SelectedContact = _dataRepository.GetContactByID(contactIndex);
            }
        }

    }
}
