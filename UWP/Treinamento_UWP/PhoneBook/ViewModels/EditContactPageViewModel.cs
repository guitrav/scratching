﻿using PhoneBook.Models;
using PhoneBook.Repositories;
using Prism.Commands;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook.ViewModels
{
    public class EditContactPageViewModel : ViewModelBase
    {
        private INavigationService _navigation;
        private IDataRepository _dataRepository;
        
        public int contactId;

        private Contact _selectedContact;
        public Contact SelectedContact
        {
            get { return _selectedContact; }
            set 
            { 
                SetProperty(ref _selectedContact, value);
                _selectedContact.ValidateProperties();
            }
        }


        private ObservableCollection<Error> _errors;
        public ObservableCollection<Error> Errors
        {
            get { return _errors; }
            set { SetProperty(ref _errors, value); }
        }


        public DelegateCommand SaveContactCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }

        public EditContactPageViewModel(INavigationService navigation, IDataRepository dataRepository)
        {
            _navigation = navigation;
            _dataRepository = dataRepository;

            Errors = new ObservableCollection<Error>();

            InitializeCommnads();
        }

        private void InitializeCommnads()
        {
            SaveContactCommand = new DelegateCommand(() => 
            {
                SelectedContact.ValidateFields();

                if (SelectedContact.Errors.Errors.Count == 0)
                {
                    _dataRepository.UpdateContact(contactId, SelectedContact);
                    _navigation.GoBack();
                }
                else
                {
                    fillErros(SelectedContact.GetAllErrors());
                }

            });

            CancelCommand = new DelegateCommand(() => _navigation.GoBack(), () => _navigation.CanGoBack());
        }

        public override void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            base.OnNavigatedTo(e, viewModelState);

            if (e.Parameter != null && e.Parameter is Contact)
            {
                var contact = (Contact)e.Parameter;
                contactId = _dataRepository.GetContactId(contact);
                SelectedContact = contact;
            }
        }


        private void fillErros(ReadOnlyDictionary<string, ReadOnlyCollection<string>> readOnlyDictionary)
        {
            Errors.Clear();

            foreach (var error in readOnlyDictionary)
            {
                Errors.Add(new Error() { Field = error.Key, Value = error.Value[0].ToString() });
            }
        }

    }
}
